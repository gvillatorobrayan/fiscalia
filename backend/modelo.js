const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// Define collection and schema for Post
let Post = new Schema({
    nombreFiscalia: {
        type: String
    },
    ubicacion: {
        type: String
    },
    direccion: {
        type: String
    },
    telefono: {
        type: String
    }
}, {
    collection: 'fiscalias'
});

module.exports = mongoose.model('Post', Post);