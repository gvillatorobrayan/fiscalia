const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const PORT = 4000;
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./DB.js');
const postRouter = require('./ruta.js');

mongoose.Promise = global.Promise;
mongoose.connect(config.DB, { useNewUrlParser: true, useUnifiedTopology: true }).then(
    () => { console.log('Base de datos Conectada (Mongo) FISCALIAS') },
    err => { console.log('No se pudo Conectar con Mongo' + err) }
);

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/fiscalias', postRouter);

app.listen(PORT, function () {
    console.log('Servidor corriendo en el puerto:', PORT);
});