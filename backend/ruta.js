const express = require('express');
const postRoutes = express.Router();

// Require Post model in our routes module
let Post = require('./modelo.js');

// Defined store route
postRoutes.route('/add').post(function (req, res) {
    let post = new Post(req.body);
    post.save()
        .then(() => {
            res.status(200).json({ 'fiscalia': 'fiscalia agregada correctamente' });
        })
        .catch(() => {
            res.status(400).send("Error al guardar en la base de datos");
        });
});

// Defined get data(index or listing) route
postRoutes.route('/').get(function (req, res) {
    Post.find(function (err, posts) {
        if (err) {
            res.json(err);
        }
        else {
            res.json(posts);
        }
    });
});

// Defined edit route
postRoutes.route('/edit/:id').get(function (req, res) {
    let id = req.params.id;
    Post.findById(id, function (err, post) {
        if (err) {
            res.json(err);
        }
        res.json(post);
    });
});

//  Defined update route
postRoutes.route('/update/:id').post(function (req, res) {
    Post.findById(req.params.id, function (err, fiscalias) {
        if (!fiscalias)
            res.status(404).send("Dato no encontrado");
        else {
            fiscalias.nombreFiscalia = req.body.nombreFiscalia;
            fiscalias.ubicacion = req.body.ubicacion;
            fiscalias.direccion = req.body.direccion;
            fiscalias.telefono = req.body.telefono;
            fiscalias.save().then(() => {
                res.json('Actualizacion completada');
            })
                .catch(() => {
                    res.status(400).send("Error no se pudo actualizar la base de datos");
                });
        }
    });
});

// Defined delete | remove | destroy route
postRoutes.route('/delete/:id').delete(function (req, res) {
    Post.findByIdAndRemove({ _id: req.params.id }, function (err) {
        if (err) res.json(err);
        else res.json('Registro eliminado correctamente');
    });
});

module.exports = postRoutes;