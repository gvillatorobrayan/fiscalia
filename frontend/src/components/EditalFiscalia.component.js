import React , { Component } from 'react';
import axios from 'axios';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

export default class EditarFiscalia extends Component {

    constructor(props) {
        super(props);

        this.state = {
            nombreFiscalia: '',
            ubicacion: '',
            direccion: '',
            telefono: ''
        }
    }

    componentDidMount() {
        axios.get('http://localhost:4000/fiscalias/edit/' + this.props.match.params.id)
            .then( res => {
                this.setState({
                    nombreFiscalia: res.data.nombreFiscalia,
                    ubicacion: res.data.ubicacion,
                    direccion: res.data.direccion,
                    telefono: res.data.telefono
                })
            })
            .catch( err => console.log(err));
    }

    onChangeNombreF = (e) => {
        this.setState({
            nombreFiscalia: e.target.value
        });
    }

    onChangeUbicacion = (e) => {
        this.setState({
            ubicacion: e.target.value
        });
    }

    onChangeDireccion = (e) => {
        this.setState({
            direccion: e.target.value
        });
    }

    onChangeTelefono = (e) => {
        this.setState({
            telefono: e.target.value
        });
    }

    onSubmit = (e) => {
        e.preventDefault();
        const obj = {
            nombreFiscalia: this.state.nombreFiscalia,
            ubicacion: this.state.ubicacion,
            direccion: this.state.direccion,
            telefono: this.state.telefono
        };
        axios.post('http://localhost:4000/fiscalias/update/' + this.props.match.params.id, obj)
            .then( res => console.log(res.data));

        this.props.history.push('/fiscalias');
    }

    render() {
        return (
            <div style={{ marginTop: "5%" }}>
               <br/>
                <form onSubmit = {this.onSubmit}>
                    
                    <div className="form-group">
                        <TextField id="outlined-size-small"
                          size="small"
                            label="Nombre Fiscalía"
                            variant="outlined"
                            type="text"
                            value={this.state.nombreFiscalia}
                                onChange={this.onChangeNombreF}
                        />
                    </div>

                    <div className="form-group" style={{ marginTop: "5%" }}>
                        <TextField id="outlined-size-small"
                          size="small"
                            label="Ubicación"
                            variant="outlined"
                            type="text"
                            value={this.state.ubicacion}
                                onChange={this.onChangeUbicacion}
                        />
                    </div>

                    <div className="form-group" style={{ marginTop: "5%" }}>
                        <TextField id="outlined-size-small"
                          size="small"
                            label="Dirección"
                            variant="outlined"
                            type="text"
                            value={this.state.direccion}
                                onChange={this.onChangeDireccion}
                        />
                    </div>

                    <div className="form-group" style={{ marginTop: "5%" }}>
                        <TextField id="outlined-size-small"
                          size="small"
                            label="Télefono"
                            variant="outlined"
                            type="text"
                            value={this.state.telefono}
                            onChange={this.onChangeTelefono}
                        />
                    </div>

                    <div className="form-group" style={{ marginTop: "5%" }}>
                        <Button type="submit" variant="contained" color="primary">
                            Actualizar Registro
                        </Button>
                    </div>
                </form>
            </div>
        )
    }
}