import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';


const Todo = props => (
    <tr>
        <td className={props.todo.todo_completed ? 'completed' : ''}>{props.todo.nombreFiscalia}</td>
        <td className={props.todo.todo_completed ? 'completed' : ''}>{props.todo.ubicacion}</td>
        <td className={props.todo.todo_completed ? 'completed' : ''}>{props.todo.direccion}</td>
        <td className={props.todo.todo_completed ? 'completed' : ''}>{props.todo.telefono}</td>
        <td>
            <Link to={"/edit/" + props.todo._id}>Editar</Link>
        </td>
    </tr>
)



export default class Fiscalias extends Component {

    constructor(props) {
        super(props);
        this.state = {
            todos: []
        };
    }

    eliminarRegistro(obj){
        if(window.confirm('¿Está seguro que desea eliminar el registro? ')){
            fetch('http://localhost:4000/fiscalias/delete/' + obj, {
                method:'DELETE',
                header:{'Accept': 'application/json',
                'Content-Type':'application/json'
                }
            })
        }
    }

    componentDidMount() {
        axios.get('http://localhost:4000/fiscalias')
            .then(res => {
                this.setState({
                    todos: res.data
                })
            })
            .catch(err => console.log(err));
    }

    componentDidUpdate() {
        axios.get('http://localhost:4000/fiscalias')
            .then(res => {
                this.setState({
                    todos: res.data
                })
            })
            .catch(err => console.log(err));
    }

    todoList = () => this.state.todos.map(
        (todo, index) => <Todo todo={todo} key={index} />
    )


    render() {
        return (
            <div>
                <br />
                <br />
                <br />
                <br />

                <TableContainer component={Paper}>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Nombre Fiscalía</TableCell>
                                <TableCell align="right">Ubicación</TableCell>
                                <TableCell align="right">Dirección</TableCell>
                                <TableCell align="right">Teléfono</TableCell>
                                <TableCell align="right">Acciones</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>

                            {this.state.todos.map((row) => (
                                <TableRow key={row._id}>
                                    <TableCell component="th" scope="row">
                                        {row.nombreFiscalia}
                                    </TableCell>
                                    <TableCell align="right">{row.ubicacion}</TableCell>
                                    <TableCell align="right">{row.direccion}</TableCell>
                                    <TableCell align="right">{row.telefono}</TableCell>
                                    <TableCell align="right">
                                        <Button variant="contained" color="primary" href={"/edit/" + row._id}>
                                            Editar
                                        </Button>
                                        <Button variant="contained" color="secondary" onClick={()=>this.eliminarRegistro(row._id)}>
                                            Eliminar
                                        </Button>
                                    </TableCell>

                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div >
        )
    }
}