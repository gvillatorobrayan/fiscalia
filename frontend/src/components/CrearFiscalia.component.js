import React, { Component } from 'react';
import axios from 'axios';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

export default class CrearFiscalia extends Component {

    constructor(props) {
        super(props);

        this.state = {
            nombreFiscalia: '',
            ubicacion: '',
            direccion:"",
            telefono: ''
        }
    }

    onChangeNombreF = e => {
        this.setState({ nombreFiscalia: e.target.value });
    }

    onChangeUbicacion = e => {
        this.setState({ ubicacion: e.target.value });
    }
    onChangeDireccion = e => {
        this.setState({ direccion: e.target.value });
    }

    onChangeTelefono = e => {
        this.setState({ telefono: e.target.value });
    }

    onSubmit = e => {
        e.preventDefault();

        // SUBMIT LOGIC NEED TO BE IMPLEMENTED HERE
        console.log('Form submitteed:');
        console.log(`Todo Nombre: ${this.state.nombreFiscalia}`);
        console.log(`Todo Ubicacion: ${this.state.ubicacion}`);
        console.log(`Todo Direccion: ${this.state.direccion}`);
        console.log(`Todo Priority: ${this.state.telefono}`);

        const newTodo = {
            nombreFiscalia: this.state.nombreFiscalia,
            ubicacion: this.state.ubicacion,
            direccion: this.state.direccion,
            telefono: this.state.telefono
        }

        axios.post('http://localhost:4000/fiscalias/add', newTodo)
            .then(res => console.log(res.data));

        this.setState({
            nombreFiscalia: '',
            ubicacion: '',
            direccion: '',
            telefono: '',
        })
        this.props.history.push('/fiscalias');
    }

    render() {
        return (
            <div style={{ marginTop: "5%" }}>
                <h3>Crear Fiscalia</h3>
                <form onSubmit={this.onSubmit} noValidate autoComplete="off">
                    <div className="form-group" >
                        <TextField id="outlined-size-small"
                          size="small"
                            label="Nombre Fiscalia"
                            variant="outlined"
                            type="text"
                          
                            value={this.state.nombreFiscalia}
                            onChange={this.onChangeNombreF}
                        />
                    </div>


                    <div className="form-group" style={{ marginTop: "5%" }}>
                        <TextField id="outlined-size-small"
                          size="small"
                            label="Municipio"
                            variant="outlined"
                            type="text"
                            value={this.state.ubicacion}
                            onChange={this.onChangeUbicacion}
                        />
                    </div>
                    <div className="form-group" style={{ marginTop: "5%" }}>
                        <TextField id="outlined-size-small"
                          size="small"
                            label="Dirección"
                            variant="outlined"
                            type="text"
                            value={this.state.direccion}
                            onChange={this.onChangeDireccion}
                        />
                    </div>
                    <div className="form-group" style={{ marginTop: "5%" }}>
                        <TextField id="outlined-size-small"
                          size="small"
                            label="Líneas Telefonicas"
                            variant="outlined"
                            type="text"
                            value={this.state.telefono}
                            onChange={this.onChangeTelefono}
                        />
                    </div>

                    <div className="form-group" style={{ marginTop: "5%" }}>
                        <Button type="submit" variant="contained" color="primary">
                            Crear Fiscalía 
                        </Button>
                    </div>
                </form>
            </div>
        )
    }
}