**MERN-Stack Fiscalia**

Esta es una aplicación que muestra una lista con diferentes Fiscalías realizada con el stack MERN (Mongo, Express, React, Node), con los cuales se puede, Agregar,Consultar, Editar y Eliminar un registro.

**Instalación**

`git clone <this_url> && cd <repo_name>`

- Instalación de dependencias para el backend

`cd backend`

`npm install`

- Instalación de dependencias para el Fronted

`cd frontend`

`npm install`

**Comenzando 🚀**

Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.

*se debe tener instalado mongodb*

`mongod`

**Entorno backend **

`cd backend`

`npm start`

**Entorno fronted**

`cd backend`

`npm start`






